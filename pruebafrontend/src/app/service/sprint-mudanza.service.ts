import { Injectable } from '@angular/core';
import {HttpClient,HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SprintMudanzaService {

  constructor(private httpClient:HttpClient) {
    
   }

  ObtenerEjecucion(formData:FormData):Observable<any>{
    return this.httpClient.post("http://localhost:9020/api/ejecucion",formData);
  }

  dowload(data){
    const REQUEST_PARAM=new HttpParams().set('fileName',data.filename)

    return this.httpClient.get("http://localhost:9020/api/dowload",{
      params:REQUEST_PARAM,
      responseType:'arraybuffer'
    });
  }
}
