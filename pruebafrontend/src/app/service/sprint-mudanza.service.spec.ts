import { TestBed } from '@angular/core/testing';

import { SprintMudanzaService } from './sprint-mudanza.service';

describe('SprintMudanzaService', () => {
  let service: SprintMudanzaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SprintMudanzaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
