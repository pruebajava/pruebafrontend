import { Component,OnInit } from '@angular/core';

import{SprintMudanzaService}from './service/sprint-mudanza.service';

import{saveAs}from 'file-saver';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Prueba Mudanza';
  submitted=false;
  cedula: number ;
  selectFile:any=File;
  json:string="";
  constructor(private springMudanzaService:SprintMudanzaService
    ){ }
  ngOnInit(): void {
  }
  onFileSelected(event){
    this.selectFile=event.target.files[0]
    
  }
  calcular(){
    const fd=new FormData();

    fd.append('file',this.selectFile)
    fd.append('cedula',this.cedula.toString())
    this.springMudanzaService.ObtenerEjecucion(fd).subscribe(resultado=>{
     
      this.dowloadfile(resultado.fileName)
    });
    
   
  }

  dowloadfile(filename){
    
    this.springMudanzaService.dowload({'filename':filename})
    .subscribe(data=>{
      saveAs(new Blob([data],{type:'text/plain'}),filename)
    })
  }
 
}
